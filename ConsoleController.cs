﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesApp
{
    static class ConsoleController
    {
        public static String InputWhichMovieDataToPrint()
        {
            Console.WriteLine("Choose information to display:");
            Console.WriteLine("1. Information about the movie");
            Console.WriteLine("2. List of movies from the range of dates");
            Console.WriteLine("3. List of movies with a selected actor");
            Console.WriteLine("");
            Console.Write("Type 1, 2 or 3 to select: ");

            String input = Console.ReadLine();
            Console.WriteLine("");

            return input;
        }

        public static void PrintSelectedData(String input)
        {
            switch (input)
            {
                case "1":
                    String movieTitle = InputMovieTitle();
                    MoviesController.GetMovie(movieTitle).PrintAllData();
                    break;
                case "2":
                    int earliestDate = InputEarliestDate();
                    int latestDate = InputLatestDate();

                    PrintMoviesInRange(earliestDate, latestDate);
                    break;
                case "3":
                    String firstName = InputFirstName();
                    String lastName = InputLastName();

                    PrintMoviesWithActor(firstName, lastName);
                    break;
                default:
                    Console.WriteLine("Incorrect input");
                    break;
            }
        }


        private static String InputMovieTitle()
        {
            Console.Write("Type name of the movie: ");
            String movieTitle = Console.ReadLine();

            return movieTitle;
        }

        private static int InputEarliestDate()
        {
            Console.Write("Type year of the earliest date: ");
            int earliestDate = Int32.Parse(Console.ReadLine());

            return earliestDate;
        }
        private static int InputLatestDate()
        {
            Console.Write("Type year of the lates date: ");
            int latestDate = Int32.Parse(Console.ReadLine());

            return latestDate;
        }
        private static void PrintMoviesInRange(int earliestDate, int latestDate)
        {
            List<Movie> moviesInRange = MoviesController.GetMoviesInYearRange(earliestDate, latestDate);
            for (int i = 0; i < moviesInRange.Count; i++)
            {
                Console.WriteLine(i + 1 + ". " + moviesInRange[i].Title);
            }
        }

        private static String InputFirstName()
        {
            Console.Write("Type first name of the actor: ");
            String firstName = Console.ReadLine();

            return firstName;
        }
        private static String InputLastName()
        {
            Console.Write("Type last name of the actor: ");
            String lastName = Console.ReadLine();

            return lastName;
        }

        private static void PrintMoviesWithActor(String firstName, String lastName)
        {
            List<Movie> moviesWithActor = MoviesController.GetMoviesWithActor(firstName, lastName);
            for (int i = 0; i < moviesWithActor.Count; i++)
            {
                Console.WriteLine(i + 1 + ". " + moviesWithActor[i].Title);
            }
        }
    }
}
