﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MoviesApp
{
    static class MoviesController
    {
        private static List<Movie> allMovies = LoadAllMoviesList();
        
        
        public static Movie GetMovie(String title)
        {
            Movie movie = new Movie();

            foreach (Movie tempMovie in allMovies)
            {
                if (tempMovie.Title.Equals(title))
                {
                    movie = tempMovie;
                    break;
                }
            }

            return movie;
        }

        public static List<Movie> GetMoviesInYearRange(int earliestYear, int latestYear)
        {
            List<Movie> moviesInRange = new List<Movie>();

            foreach (Movie tempMovie in allMovies)
                {
                    if (earliestYear <= tempMovie.ReleaseYear && tempMovie.ReleaseYear <= latestYear)
                    {
                    moviesInRange.Add(tempMovie);
                    }
                }

            return moviesInRange;
        }

        public static List<Movie> GetMoviesWithActor(String firstName, String lastName)
        {
            List<Movie> moviesWithActor = new List<Movie>();

            foreach (Movie tempMovie in allMovies)
            {
                
                if (tempMovie.ContainsActor(firstName, lastName))
                {
                    moviesWithActor.Add(tempMovie);
                }
            }

            return moviesWithActor;
        }


        private static List<Movie> LoadAllMoviesList()
        {
            String moviesJSONPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, @"json\movie.json");
            StreamReader r = new StreamReader(moviesJSONPath);
            String moviesJson = r.ReadToEnd();

            List<Movie> allMovies = JsonConvert.DeserializeObject<List<Movie>>(moviesJson);

            return allMovies;
        }
    }
}
