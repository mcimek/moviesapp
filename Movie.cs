﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MoviesApp
{
    class Movie
    {
        public String Title { get; set; }
        public int ReleaseYear { get; set; }
        private List<String> actorsIDsList;
        private static List<Actor> allActors = LoadAllActorsList();
        private List<Actor> actorsList = new List<Actor>();
        
        public List<String> ActorsIDs
        {
            get { return actorsIDsList; }
            set
            {
                actorsIDsList = value;
                SetActualActorsList();
            }
        }
        public List<Actor> Actors
        {
            get { return actorsList; }
        }


        public void PrintAllData()
        {
            Console.WriteLine("Title: " + Title);
            Console.WriteLine("Release date: " + ReleaseYear);
            Console.WriteLine("Actors:");
            for (int i = 0; i < Actors.Count; i++)
            {
                Actor actor = Actors[i];
                Console.WriteLine(i+1 + ". " + actor.FirstName + " " + actor.LastName);
            }
        }

        public bool ContainsActor(String firstName, String lastName)
        {
            bool actorExists = false;

            foreach (Actor tempActor in Actors)
            {
                if (tempActor.FirstName.Equals(firstName) && tempActor.LastName.Equals(lastName))
                {
                    actorExists = true;
                    break;
                }
            }

            return actorExists;
        }


        private static List<Actor> LoadAllActorsList()
        {
            String actorsJSONPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, @"json\actor.json");
            StreamReader r = new StreamReader(actorsJSONPath);
            String actorsJson = r.ReadToEnd();

            List<Actor> allActors = JsonConvert.DeserializeObject<List<Actor>>(actorsJson);

            return allActors;
        }

        private void SetActualActorsList()
        {
            actorsList = new List<Actor>();
            
            foreach(Actor tempActor in allActors)
            {
                if (ActorsIDs.Contains(tempActor.Id))
                {
                    actorsList.Add(tempActor);
                }
            }
        }
    }
}
