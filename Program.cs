﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace MoviesApp
{
    class Program
    {
        static void Main(string[] args)
        {
            String input = ConsoleController.InputWhichMovieDataToPrint();
            ConsoleController.PrintSelectedData(input);
            
            Console.ReadKey();
        }
    }
}
